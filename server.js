const express = require('express');
const serveStatic = require('serve-static');
const path = require('path');

const app = express();

app.use('/', serveStatic(path.join(__dirname, '/dist')));
app.use('/login', serveStatic(path.join(__dirname, '/dist')));
app.use('/announcement/:id', serveStatic(path.join(__dirname, '/dist')));
app.use('/book/:id', serveStatic(path.join(__dirname, '/dist')));
app.use('/myAnnouncement/:id', serveStatic(path.join(__dirname, '/dist')));
app.use('/myAnnouncementBook/:id', serveStatic(path.join(__dirname, '/dist')));
app.use('/privacy', serveStatic(path.join(__dirname, '/dist')));

const port = process.env.PORT || 8080;
app.listen(port);