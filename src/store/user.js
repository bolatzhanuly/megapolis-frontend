import axios from "axios";

export default {
    state: {
        user: {}
    },
    mutations: {
        setUser (state, user) {
            state.user = user;
            localStorage.setItem('user', JSON.stringify(user));
        },
        clearUser (state) {
            state.user = {};
            localStorage.removeItem('user');
        }
    },
    actions: {
        async fetchUser ( {commit} ) {
            try {
                const token = 'Token ' + localStorage.getItem('userToken');
                const user = await axios.get('https://megapolus.herokuapp.com/auth/v1/users/me/', {
                    headers: { Authorization: token }
                });
                commit("setUser", user.data);
                return user.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    },
    getters: {
        getUser (state) {
            if (state.user.length) {
                return state.user
            } else {
                let user = localStorage.getItem('user');
                user = JSON.parse(user);
                return user
            }
        }
    }
}