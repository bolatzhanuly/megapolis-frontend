import axios from "axios";

export default {
    state: {
        userToken: null
    },
    mutations: {
        setUserToken (state, userToken) {
            state.userToken = userToken;
            localStorage.setItem('userToken', userToken);
        },
        clearUserToken (state) {
            state.userToken = null;
            localStorage.removeItem('userToken');
        }
    },
    actions: {
        async register ( {dispatch}, credentials ) {
            try {
                await axios.post('https://megapolus.herokuapp.com/auth/v1/users/', credentials);
                await dispatch('login', {
                    username: credentials.username,
                    password: credentials.password
                })
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        async login ( {commit}, credentials ) {
            try {
                const data = await axios.post('https://megapolus.herokuapp.com/auth/v1/token/login/', credentials);
                commit('setUserToken', data.data.auth_token);
                // eslint-disable-next-line no-empty
            } catch (e) {}

        },
        async logout ( {commit} ) {
            let token = 'Token ' + localStorage.getItem('userToken');
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': token
            };
            try {
                await axios.post('https://megapolus.herokuapp.com/auth/v1/token/logout/', '', {
                        headers: headers
                    });
                commit('clearUserToken');
                commit('clearUser');
                commit('clearCurrentAnnouncement');
                commit('clearCurrentMyAnnouncement');
                commit('clearCurrentMyBook');
                commit('clearCurrentMyAnnouncementBookUrl');
                commit('clearCurrentMyBookUrl');
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    },
    getters: {
        userLoggedIn (state) {
            return !!state.userToken || !!localStorage.getItem('userToken');
        }
    }
}