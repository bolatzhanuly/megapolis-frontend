export default {
    actions: {
        async fetchMyBooks ( {dispatch, getters} ) {
            try {
                let myBooks = await dispatch('fetchBooks');
                let currentUser = getters.getUser.id;
                myBooks = myBooks.filter(book => book.user === currentUser);
                return myBooks
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    }
}