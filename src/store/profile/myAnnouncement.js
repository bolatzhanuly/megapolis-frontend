import axios from "axios";

export default {
    state: {
        currentMyAnnouncementUrl: ''
    },
    mutations: {
        setCurrentMyAnnouncement (state, announcementUrl) {
            state.currentMyAnnouncementUrl = announcementUrl;
            localStorage.setItem('currentMyAnnouncementUrl', announcementUrl);
        },
        clearCurrentMyAnnouncement (state) {
            state.currentMyAnnouncementUrl = {};
            localStorage.removeItem('currentMyAnnouncementUrl');
        },
    },
    actions: {
        async fetchCurrentMyAnnouncement ( {dispatch, getters} ) {
            try {
                const currentAnnouncementUrl = getters.getCurrentMyAnnouncementUrl;
                const currentAnnouncement = await dispatch('fetchCurrentAnnouncement', currentAnnouncementUrl);
                return currentAnnouncement
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        setCurrentMyAnnouncement ( {commit}, announcementUrl) {
            commit('setCurrentMyAnnouncement', announcementUrl)
        },
        // eslint-disable-next-line no-empty-pattern
        async changeStatusOfAnnouncement ( {}, payload) {
            try {
                await axios.patch(payload.announcementUrl, {
                    enabled: payload.enabled
                })
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        // eslint-disable-next-line no-empty-pattern
        async changeStatusOfBookingOfThisAnnouncement ( {}, payload) {
            try {
                await axios.patch(payload.announcementUrl, {
                    booking_enabled: payload.booking_enabled
                })
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    },
    getters: {
        getCurrentMyAnnouncementUrl (state) {
            if (!state.currentMyAnnouncementUrl) {
                return localStorage.getItem('currentMyAnnouncementUrl')
            } else {
                return state.currentMyAnnouncementUrl
            }
        }
    }
}