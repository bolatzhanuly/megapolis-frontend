import axios from "axios";

export default {
    actions: {
        async fetchMyAnnouncements ( {dispatch, getters} ) {
            try {
                let myAnnouncements = await dispatch('fetchAnnouncements');
                let userId = getters.getUser.id;
                myAnnouncements = myAnnouncements.filter(announcement => announcement.user === userId);
                return myAnnouncements
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        // eslint-disable-next-line no-empty-pattern
        async deleteMyAnnouncement ( {}, announcementUrl) {
            try {
                await axios.delete(announcementUrl);
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    }
}