import axios from "axios";

export default {
    state: {
        currentMyBook: {},
        currentMyBookUrl: ''
    },
    mutations: {
        setCurrentMyBook (state, currentMyBook) {
            state.currentMyBook = currentMyBook;
            localStorage.setItem('currentMyBook', JSON.stringify(currentMyBook));
        },
        clearCurrentMyBook (state) {
            state.currentMyBook = {};
            localStorage.removeItem('currentMyBook');
        },
        setCurrentMyBookUrl (state, currentMyBookUrl) {
            state.currentMyBookUrl = currentMyBookUrl;
            localStorage.setItem('currentMyBookUrl', currentMyBookUrl);
        },
        clearCurrentMyBookUrl (state) {
            state.currentMyBookUrl = {};
            localStorage.removeItem('currentMyBookUrl');
        },
    },
    actions: {
        async fetchCurrentMyBook ( {commit}, bookUrl ) {
            try {
                const currentMyBook = await axios.get(bookUrl);
                const currentMyBookUrl = "https://megapolus.herokuapp.com/booking/" + currentMyBook.data.id + "/";
                commit('setCurrentMyBook', currentMyBook.data);
                commit('setCurrentMyBookUrl', currentMyBookUrl);
                return currentMyBook.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        // eslint-disable-next-line no-empty-pattern
        async deleteMyBook ( {}, myBookUrl ) {
            await axios.delete(myBookUrl)
        }
    },
    getters: {
        getCurrentMyBook (state) {
            if (state.currentMyBook.length) {
                return state.currentMyBook
            } else {
                let currentMyBook = localStorage.getItem('currentMyBook');
                currentMyBook = JSON.parse(currentMyBook);
                return currentMyBook
            }
        },
        getCurrentMyBookUrl (state) {
            if (state.currentMyBookUrl.length) {
                return state.currentMyBookUrl
            } else {
                let currentMyBookUrl = localStorage.getItem('currentMyBookUrl');
                return currentMyBookUrl
            }
        }
    }
}