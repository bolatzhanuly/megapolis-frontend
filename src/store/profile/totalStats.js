export default {
    actions: {
        async calcAllMyIncome ( {dispatch} ) {
            let myAnnouncements = await dispatch('fetchMyAnnouncements');
            let myAnnouncementsBooks = await dispatch('fetchMyAnnouncementsBooks');

            let newMyAnnouncementsBooks = [];
            let statuses = [];
            let date1, date2, diffTime, diffDays, allMyIncome = 0;

            for (let i = 0; i < myAnnouncementsBooks.length; i++) {
                date1 = new Date(`${myAnnouncementsBooks[i].date_start}T00:00:00`);
                date2 = new Date(`${myAnnouncementsBooks[i].date_end}T23:59:59`);
                diffTime = Math.abs(date2 - date1);
                diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                newMyAnnouncementsBooks.push({
                    days: diffDays,
                    announcement: myAnnouncementsBooks[i].apartment,
                    statuses: myAnnouncementsBooks[i].statuses
                })
            }

            for (let i = 0; i < newMyAnnouncementsBooks.length; i++){
                statuses = newMyAnnouncementsBooks[i].statuses.filter(status => status.term === 6);
                if (statuses.length) {
                    for (let j = 0; j < myAnnouncements.length; j++) {
                        if (myAnnouncements[j].id === newMyAnnouncementsBooks[i].announcement) {
                            allMyIncome += myAnnouncements[j].price * newMyAnnouncementsBooks[i].days;
                        }
                    }
                }
            }

            return allMyIncome
        },
        async calcAllMyOutcome ( {dispatch} ) {
            let myBooks = await dispatch('fetchMyBooks');

            let newMyBooks = [];
            let statuses = [];
            let announcement = {};
            let date1, date2, diffTime, diffDays, allMyOutcome = 0, announcementUrl;

            for (let i = 0; i < myBooks.length; i++) {
                date1 = new Date(`${myBooks[i].date_start}T00:00:00`);
                date2 = new Date(`${myBooks[i].date_end}T23:59:59`);
                diffTime = Math.abs(date2 - date1);
                diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                newMyBooks.push({
                    days: diffDays,
                    announcement: myBooks[i].apartment,
                    statuses: myBooks[i].statuses
                })
            }

            for (let i = 0; i < newMyBooks.length; i++){
                statuses = newMyBooks[i].statuses.filter(status => status.term === 6);
                if (statuses.length) {
                    announcementUrl = "https://megapolus.herokuapp.com/apartments/" + newMyBooks[i].announcement + "/";
                    announcement = await dispatch('fetchCurrentAnnouncement', announcementUrl);
                    allMyOutcome += newMyBooks[i].days * announcement.price;
                }
            }

            return allMyOutcome

        },
        async calcMyIncomeByMonth ( {dispatch}, month ) {
            let myAnnouncementsBooks = await dispatch('fetchMyAnnouncementsBooks');

            let newMyAnnouncementsBooks = [];
            let statuses = [];
            let announcement = {};
            let date1, date2, diffTime, diffDays, announcementUrl, incomeByMonth = 0;

            if (month < 10) {
                for (let i = 0; i < myAnnouncementsBooks.length; i++) {
                    date1 = new Date(`${myAnnouncementsBooks[i].date_start}T00:00:00`);
                    date2 = new Date(`${myAnnouncementsBooks[i].date_end}T23:59:59`);
                    diffTime = Math.abs(date2 - date1);
                    diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    newMyAnnouncementsBooks.push({
                        date_end: myAnnouncementsBooks[i].date_end,
                        days: diffDays,
                        announcement: myAnnouncementsBooks[i].apartment,
                        statuses: myAnnouncementsBooks[i].statuses
                    });

                    for (let i = 0; i < newMyAnnouncementsBooks.length; i++){
                        if(parseInt(newMyAnnouncementsBooks[i].date_end.charAt(6)) === month){
                            statuses = newMyAnnouncementsBooks[i].statuses.filter(status => status.term === 6);
                            if (statuses.length) {
                                announcementUrl = "https://megapolus.herokuapp.com/apartments/" + newMyAnnouncementsBooks[i].announcement + "/";
                                announcement = await dispatch('fetchCurrentAnnouncement', announcementUrl);
                                incomeByMonth += newMyAnnouncementsBooks[i].days * announcement.price;
                            }
                        }
                    }
                }
            }

            return incomeByMonth
        }
    }
}