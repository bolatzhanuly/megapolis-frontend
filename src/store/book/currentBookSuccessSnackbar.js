export default {
    state: {
        currentBookSuccessSnackbar: false
    },
    mutations: {
        changeBookSuccessSnackbarToTrue (state) {
            state.currentBookSuccessSnackbar = true
        },
        changeBookSuccessSnackbarToFalse (state) {
            state.currentBookSuccessSnackbar = false
        },
    },
    actions: {
        changeBookSuccessSnackbarToTrue ( {commit} ) {
            commit('changeBookSuccessSnackbarToTrue')
        },
        changeBookSuccessSnackbarToFalse ( {commit} ) {
            commit('changeBookSuccessSnackbarToFalse')
        }
    },
    getters: {
        currentBookSuccessSnackbar (state) {
            return state.currentBookSuccessSnackbar
        }
    }
}