import axios from "axios";

export default {
    actions: {
        // eslint-disable-next-line no-empty-pattern
        async addBook ( {}, credentials ) {
            try {
                const book = await axios.post('https://megapolus.herokuapp.com/booking/', credentials);
                return book.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        async fetchBooks () {
            try {
                const books = await axios.get('https://megapolus.herokuapp.com/booking/');
                return books.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        async fetchBooksByAnnouncementId ( {dispatch}, announcementId ) {
            try {
                let books = await dispatch('fetchBooks');
                books = books.filter(book => book.apartment === announcementId);
                return books
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
    }
}