export default {
    state: {
        currentBookDialogStatus: false
    },
    mutations: {
        changeBookDialogStatusToTrue (state) {
            state.currentBookDialogStatus = true
        },
        changeBookDialogStatusToFalse (state) {
            state.currentBookDialogStatus = false
        }
    },
    actions: {
        changeBookDialogStatusToTrue ( {commit} ) {
            commit('changeBookDialogStatusToTrue')
        },
        changeBookDialogStatusToFalse ( {commit} ) {
            commit('changeBookDialogStatusToFalse')
        }
     },
    getters: {
        currentBookDialogStatus (state) {
            return state.currentBookDialogStatus
        }
    }
}