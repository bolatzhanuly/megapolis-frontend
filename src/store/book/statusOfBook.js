import axios from "axios";

export default {
    actions: {
        // eslint-disable-next-line no-empty-pattern
        async addStatusOfBook ( {}, payload ) {
            try {
                await axios.post('https://megapolus.herokuapp.com/statuses/', {
                    term: payload.term,
                    booking: payload.booking,
                })
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    }
}