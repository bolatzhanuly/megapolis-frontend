import Vue from 'vue'
import Vuex from 'vuex'
import apartments from "./announcement/announcements";
import auth from "./auth";
import user from "./user";
import book from "./book/book";
import home from "./book/bookDialogStatus";
import currentBookSuccessSnackbar from "./book/currentBookSuccessSnackbar";
import filteredApartments from "./announcement/filteredAnnouncements";
import myBooks from "./profile/myBooks";
import myBook from "./profile/myBook";
import myAnnouncements from "./profile/myAnnouncements";
import myAnnouncement from "./profile/myAnnouncement";
import currentAnnouncement from "./announcement/currentAnnouncement"
import statusOfBook from "./book/statusOfBook"
import myAnnouncementBooks from "./myAnnouncement/myAnnouncementBooks"
import myAnnouncementBook from "./myAnnouncement/myAnnouncementBook"
import totalStats from "./profile/totalStats";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    apartments,
    auth,
    user,
    book,
    home,
    currentBookSuccessSnackbar,
    filteredApartments,
    myBooks,
    myBook,
    myAnnouncements,
    myAnnouncement,
    currentAnnouncement,
    statusOfBook,
    myAnnouncementBooks,
    myAnnouncementBook,
    totalStats
  }
})
