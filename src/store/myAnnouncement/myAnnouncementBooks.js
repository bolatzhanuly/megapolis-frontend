export default {
    actions: {
        async fetchMyAnnouncementBooks ( {dispatch, getters} ) {
            try {
                let myAnnouncementBooks = await dispatch('fetchBooks');
                let currentMyAnnouncementId = getters.getCurrentAnnouncement.id;
                myAnnouncementBooks = myAnnouncementBooks.filter(book => book.apartment === currentMyAnnouncementId);
                return myAnnouncementBooks
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        async fetchMyAnnouncementsBooks ( {dispatch, getters} ) {
            try {
                let myAnnouncementsBooks = await dispatch('fetchBooks');
                let userId = getters.getUser.id;
                myAnnouncementsBooks = myAnnouncementsBooks.filter(book => parseInt(book.apartment_owner) === userId);
                return myAnnouncementsBooks
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    }
}