import axios from "axios";

export default {
    state: {
        currentMyAnnouncementBookUrl: '',
    },
    mutations: {
        setCurrentMyAnnouncementBookUrl (state, currentMyAnnouncementBookUrl) {
            state.currentMyAnnouncementBookUrl = currentMyAnnouncementBookUrl;
            localStorage.setItem('currentMyAnnouncementBookUrl', currentMyAnnouncementBookUrl);
        },
        clearCurrentMyAnnouncementBookUrl (state) {
            state.currentMyAnnouncementBookUrl = '';
            localStorage.removeItem('currentMyAnnouncementBookUrl');
        },
    },
    actions: {
        async fetchCurrentMyAnnouncementBook ( {commit}, bookUrl ) {
            try {
                const currentMyAnnouncementBook = await axios.get(bookUrl);
                const currentMyAnnouncementBookUrl = "https://megapolus.herokuapp.com/booking/" + currentMyAnnouncementBook.data.id + "/";
                commit('setCurrentMyAnnouncementBookUrl', currentMyAnnouncementBookUrl);
                return currentMyAnnouncementBook.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    },
    getters: {
        getCurrentMyAnnouncementBookUrl (state) {
            if (state.currentMyAnnouncementBookUrl.length) {
                return state.currentMyAnnouncementBookUrl
            } else {
                let currentMyAnnouncementBookUrl = localStorage.getItem('currentMyAnnouncementBookUrl');
                return currentMyAnnouncementBookUrl
            }
        }
    }
}