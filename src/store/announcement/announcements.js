import axios from "axios";

export default {
    actions: {
        // eslint-disable-next-line no-empty-pattern
        async addAnnouncement ( {}, data ) {
            try {
                const announcement = await axios({
                    method: "POST",
                    url: "https://megapolus.herokuapp.com/apartments/",
                    data: data,
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                });
                return announcement.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        },
        async fetchAnnouncements () {
            try {
                const announcements = await axios.get('https://megapolus.herokuapp.com/apartments/');
                return announcements.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    }
}