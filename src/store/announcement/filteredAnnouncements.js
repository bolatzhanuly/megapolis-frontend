export default {
    state: {
        filteredAnnouncements: []
    },
    mutations: {
      setFilteredAnnouncements (state, filteredAnnouncements) {
          state.filteredAnnouncements = filteredAnnouncements;
      }
    },
    actions: {
        async fetchFilteredAnnouncements ( {commit, dispatch}, filterConditions ) {
            try {
                let announcements = await dispatch('fetchAnnouncements');
                let user = await dispatch('fetchUser');

                let result = () => {
                    if (filterConditions.selectedRooms.length > 0) {
                        let filteredAnnouncementsByRooms = [];
                        for (let i in filterConditions.selectedRooms) {
                            for (let j in announcements) {
                                if (filterConditions.selectedRooms[i] === announcements[j].numberOfRooms){
                                    filteredAnnouncementsByRooms.push(announcements[j])
                                }
                            }
                        }
                        announcements = filteredAnnouncementsByRooms
                    }

                    if (filterConditions.fromPrice) {
                        announcements = announcements.filter(announcement => announcement.price > parseInt(filterConditions.fromPrice));
                    }

                    if (filterConditions.toPrice) {
                        announcements = announcements.filter(announcement => announcement.price < parseInt(filterConditions.toPrice));
                    }

                    if (filterConditions.photo) {
                        announcements = announcements.filter(announcement => announcement.img_link !== "https://xn----7sbb1aspsel4gqaqr.xn--p1ai/static/empty.png");
                    }

                    announcements = announcements.filter(announcement => announcement.enabled === true);

                    return announcements
                };

                if (user) {
                    announcements = announcements.filter(announcement => announcement.user !== user.id);
                    result();
                } else if (!user) {
                    result();
                 }

                commit("setFilteredAnnouncements", announcements);
                return announcements

                // eslint-disable-next-line no-empty
            } catch (e) {}

        }
    },
    getters: {
        getFilteredAnnouncements (state) {
            return state.filteredAnnouncements
        }
    }

}