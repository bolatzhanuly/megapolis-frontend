import axios from "axios";

export default {
    state: {
        currentAnnouncement: {}
    },
    mutations: {
        setCurrentAnnouncement (state, currentAnnouncement) {
            state.currentAnnouncement = currentAnnouncement;
            localStorage.setItem('currentAnnouncement', JSON.stringify(currentAnnouncement));
        },
        clearCurrentAnnouncement (state) {
            state.currentAnnouncement = [];
            localStorage.removeItem('currentAnnouncement');
        },
    },
    actions: {
        async fetchCurrentAnnouncement ( {commit}, announcementUrl ) {
            try {
                const currentAnnouncement = await axios.get(announcementUrl);
                commit('setCurrentAnnouncement', currentAnnouncement.data);
                return currentAnnouncement.data
                // eslint-disable-next-line no-empty
            } catch (e) {}
        }
    },
    getters: {
        getCurrentAnnouncement (state) {
            if (state.currentAnnouncement.length) {
                return state.currentAnnouncement
            } else {
                let currentAnnouncement = localStorage.getItem('currentAnnouncement');
                currentAnnouncement = JSON.parse(currentAnnouncement);
                return currentAnnouncement
            }
        }
    }
}