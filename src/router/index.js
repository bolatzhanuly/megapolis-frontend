import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/announcement/Home.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/user/Login.vue')
  },
  {
    path: '/registration',
    name: 'Registration',
    component: () => import('@/views/user/Registration.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('@/views/user/Profile.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/addAnnouncement',
    name: 'AddAnnouncement',
    component: () => import('@/views/announcement/AddAnnouncement.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/announcement/:id',
    name: 'Announcement',
    component: () => import('@/views/announcement/Announcement.vue')
  },
  {
    path: '/book/:id',
    name: 'MyBook',
    component: () => import('@/views/profile/MyBook.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/myAnnouncement/:id',
    name: 'MyAnnouncement',
    component: () => import('@/views/profile/MyAnnouncement.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/myAnnouncementBook/:id',
    name: 'MyAnnouncementBook',
    component: () => import('@/views/myAnnouncement/MyAnnouncementBook.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/privacy',
    name: 'Privacy',
    component: () => import('@/views/Policy.vue')
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem('userToken');

  if(to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next('/login')
  }
  next()
});

export default router
